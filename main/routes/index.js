const routes = require('express').Router();

routes.get('/', (req, res) => {
  res.status(200).json({ APP: 'Wunder Queue Messaging System', Description: 'Hit /msg to launch' });
});

module.exports = routes;
