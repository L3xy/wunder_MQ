import http from 'http';
import amqp from 'amqplib/callback_api';

import config from './config';

const app = require('express')();
const routes = require('./routes');

//Create the app
app.server = http.createServer(app);

app.use('/', routes);

//MESSAGING SYSTEM MIDDLEWARE
app.use('/msg', (req, res) => {

  let amqpConn = null;
  function start() {
      amqp.connect(`${config.cloudamqpURL}?heartbeat=60`, (err, conn) => {
        if (err) {
          console.error("[wunderMQP]", err.message);
          return setTimeout(start, 1000);
        }
        conn.on("error", err => {
          if (err.message !== "Connection closing") {
            console.error("[wunderMQP] conn error", err.message);
          }
        });
        conn.on("close", () => {
          console.error("[wunderMQP] reconnecting");
          return setTimeout(start, 1000);
        });

        console.log("[wunderMQP] connected");
      amqpConn = conn;

      whenConnected();
    });
  }

  function whenConnected() {
    startPublisher();
    startWorker();
  }

  let pubChannel = null;
  const offlinePubQueue = [];
  function startPublisher() {
    amqpConn.createConfirmChannel((err, ch) => {
      if (closeOnErr(err)) return;
      ch.on("error", err => {
        console.error("[wunderMQP] channel error", err.message);
      });
      ch.on("close", () => {
        console.log("[wunderMQP] channel closed");
      });

      pubChannel = ch;
      while (true) {
        const m = offlinePubQueue.shift();
        if (!m) break;
        publish(m[0], m[1], m[2]);
      }
    });
  }

  // method to publish a message, will queue messages internally if the connection is down and resend later
  function publish(exchange, routingKey, content) {
    try {
      pubChannel.publish(exchange, routingKey, content, { persistent: true },
                         (err) => {
                           if (err) {
                             console.error("[wunderMQP] publish", err);
                             offlinePubQueue.push([exchange, routingKey, content]);
                             pubChannel.connection.close();
                           }
                         });
    } catch (e) {
      console.error("[wunderMQP] publish", e.message);
      offlinePubQueue.push([exchange, routingKey, content]);
    }
  }

  // A worker that acks messages only if processed succesfully
  function startWorker() {
    amqpConn.createChannel((err, ch) => {
      if (closeOnErr(err)) return;
      ch.on("error", err => {
        console.error("[wunderMQP] channel error", err.message);
      });
      ch.on("close", () => {
        console.log("[wunderMQP] channel closed");
      });
      ch.prefetch(10);
      ch.assertQueue("jobs", { durable: true }, (err) => {
        if (closeOnErr(err)) return;
        ch.consume("jobs", processMsg, { noAck: false });
        console.log("Worker is started");
      });

      function processMsg(msg) {
        work(msg, ok => {
          try {
            if (ok)
              ch.ack(msg);
            else
              ch.reject(msg, true);
          } catch (e) {
            closeOnErr(e);
          }
        });
      }
    });
  }

  function work(msg, cb) {
    console.log("Message received!", msg.content.toString());
    cb(true);
  }

  function closeOnErr(err) {
    if (!err) return false;
    console.error("[wunderMQP] error", err);
    amqpConn.close();
    return true;
  }

  setInterval(() => {
    publish("", "jobs", new Buffer("...Getting to work!"));
  }, 1000);

  start();

});


app.server.listen(config.port); //setting the port to whats in config file
console.log(`Node Server started on port ${app.server.address().port}`);

export default app;
