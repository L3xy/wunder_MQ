import amqp from 'amqplib/callback_api';
import config from '../config';
const route = require('../routes');

//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../index');
let baseURL = 'http://localhost:3005';
let should = chai.should();
let expect = require('chai').expect;


chai.use(chaiHttp);

describe('/GET home', () => {
  it('it should get to the home route', done => {
    chai.request(baseURL)
      .get('/')
      .end((err, res) => {
        res.should.have.status(200);
        done();
      });
    });
});

describe('Connection', () => {
  it('it should connect to cloudamqp', done => {
    amqp.connect(`${config.cloudamqpURL}?heartbeat=60`, (err, conn) => {
      expect(err).to.be.null;
      done();
    });
  });
});
