'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _http = require('http');

var _http2 = _interopRequireDefault(_http);

var _callback_api = require('amqplib/callback_api');

var _callback_api2 = _interopRequireDefault(_callback_api);

var _config = require('./config');

var _config2 = _interopRequireDefault(_config);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var app = require('express')();
var routes = require('./routes');

//Create the app
app.server = _http2.default.createServer(app);

app.use('/', routes);

//MESSAGING SYSTEM MIDDLEWARE
app.use('/msg', function (req, res) {

  var amqpConn = null;
  function start() {
    _callback_api2.default.connect(_config2.default.cloudamqpURL + '?heartbeat=60', function (err, conn) {
      if (err) {
        console.error("[wunderMQP]", err.message);
        return setTimeout(start, 1000);
      }
      conn.on("error", function (err) {
        if (err.message !== "Connection closing") {
          console.error("[wunderMQP] conn error", err.message);
        }
      });
      conn.on("close", function () {
        console.error("[wunderMQP] reconnecting");
        return setTimeout(start, 1000);
      });

      console.log("[wunderMQP] connected");
      amqpConn = conn;

      whenConnected();
    });
  }

  function whenConnected() {
    startPublisher();
    startWorker();
  }

  var pubChannel = null;
  var offlinePubQueue = [];
  function startPublisher() {
    amqpConn.createConfirmChannel(function (err, ch) {
      if (closeOnErr(err)) return;
      ch.on("error", function (err) {
        console.error("[wunderMQP] channel error", err.message);
      });
      ch.on("close", function () {
        console.log("[wunderMQP] channel closed");
      });

      pubChannel = ch;
      while (true) {
        var m = offlinePubQueue.shift();
        if (!m) break;
        publish(m[0], m[1], m[2]);
      }
    });
  }

  // method to publish a message, will queue messages internally if the connection is down and resend later
  function publish(exchange, routingKey, content) {
    try {
      pubChannel.publish(exchange, routingKey, content, { persistent: true }, function (err) {
        if (err) {
          console.error("[wunderMQP] publish", err);
          offlinePubQueue.push([exchange, routingKey, content]);
          pubChannel.connection.close();
        }
      });
    } catch (e) {
      console.error("[wunderMQP] publish", e.message);
      offlinePubQueue.push([exchange, routingKey, content]);
    }
  }

  // A worker that acks messages only if processed succesfully
  function startWorker() {
    amqpConn.createChannel(function (err, ch) {
      if (closeOnErr(err)) return;
      ch.on("error", function (err) {
        console.error("[wunderMQP] channel error", err.message);
      });
      ch.on("close", function () {
        console.log("[wunderMQP] channel closed");
      });
      ch.prefetch(10);
      ch.assertQueue("jobs", { durable: true }, function (err) {
        if (closeOnErr(err)) return;
        ch.consume("jobs", processMsg, { noAck: false });
        console.log("Worker is started");
      });

      function processMsg(msg) {
        work(msg, function (ok) {
          try {
            if (ok) ch.ack(msg);else ch.reject(msg, true);
          } catch (e) {
            closeOnErr(e);
          }
        });
      }
    });
  }

  function work(msg, cb) {
    console.log("Message received!", msg.content.toString());
    cb(true);
  }

  function closeOnErr(err) {
    if (!err) return false;
    console.error("[wunderMQP] error", err);
    amqpConn.close();
    return true;
  }

  setInterval(function () {
    publish("", "jobs", new Buffer("...Getting to work!"));
  }, 1000);

  start();
});

app.server.listen(_config2.default.port); //setting the port to whats in config file
console.log('Node Server started on port ' + app.server.address().port);

exports.default = app;
//# sourceMappingURL=index.js.map