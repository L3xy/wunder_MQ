'use strict';

var routes = require('express').Router();

routes.get('/', function (req, res) {
  res.status(200).json({ APP: 'Wunder Queue Messaging System', Description: 'Hit /msg to launch' });
});

module.exports = routes;
//# sourceMappingURL=index.js.map