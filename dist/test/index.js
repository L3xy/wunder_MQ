'use strict';

var _callback_api = require('amqplib/callback_api');

var _callback_api2 = _interopRequireDefault(_callback_api);

var _config = require('../config');

var _config2 = _interopRequireDefault(_config);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var route = require('../routes');

//Require the dev-dependencies
var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../index');
var baseURL = 'http://localhost:3005';
var should = chai.should();
var expect = require('chai').expect;

chai.use(chaiHttp);

describe('/GET home', function () {
  it('it should get to the home route', function (done) {
    chai.request(baseURL).get('/').end(function (err, res) {
      res.should.have.status(200);
      done();
    });
  });
});

describe('Connection', function () {
  it('it should connect to cloudamqp', function (done) {
    _callback_api2.default.connect(_config2.default.cloudamqpURL + '?heartbeat=60', function (err, conn) {
      expect(err).to.be.null;
      done();
    });
  });
});
//# sourceMappingURL=index.js.map